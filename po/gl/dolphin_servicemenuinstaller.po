# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the dolphin package.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: dolphin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-02 00:46+0000\n"
"PO-Revision-Date: 2019-08-16 11:13+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: servicemenuinstaller.cpp:44
#, kde-format
msgid "Dolphin service menu installation failed"
msgstr "A instalación do menú de servizo de Dolphin fallou"

#: servicemenuinstaller.cpp:72
#, kde-format
msgid "Failed to install \"%1\", exited with status \"%2\""
msgstr ""

#: servicemenuinstaller.cpp:104
#, fuzzy, kde-format
#| msgid "Failed to run installer script %1"
msgid "Failed to uninstall \"%1\", exited with status \"%2\""
msgstr "Non se puido executar o script de instalador %1"

#: servicemenuinstaller.cpp:117
#, kde-format
msgid "The file does not exist!"
msgstr ""

#: servicemenuinstaller.cpp:126
#, kde-format
msgid "Unknown error when installing package"
msgstr ""

#: servicemenuinstaller.cpp:181
#, kde-format
msgid "Unsupported archive type %1: %2"
msgstr "Non se admite o tipo de arquivo %1: %2"

#: servicemenuinstaller.cpp:190
#, kde-format
msgid "Failed to run uncompressor command for %1"
msgstr "Non se puido executar a orde de descompresor para %1"

#: servicemenuinstaller.cpp:195
#, kde-format
msgid "Process did not finish in reasonable time: %1 %2"
msgstr "O proceso non rematou nun tempo razoábel: %1 %2"

#: servicemenuinstaller.cpp:199
#, kde-format
msgid "Failed to uncompress %1"
msgstr "Non se puido descomprimir %1"

#: servicemenuinstaller.cpp:232
#, kde-format
msgid "Failed to run installer script %1"
msgstr "Non se puido executar o script de instalador %1"

#: servicemenuinstaller.cpp:255
#, kde-format
msgid "Failed to set permissions on %1: %2"
msgstr "Non se puideron definir os permisos en %1: %2"

#: servicemenuinstaller.cpp:272
#, kde-format
msgctxt "Separator between arguments"
msgid "\", \""
msgstr "\", \""

#: servicemenuinstaller.cpp:272
#, kde-format
msgctxt "%2 = comma separated list of arguments"
msgid "Installer script %1 failed, tried arguments \"%2\"."
msgstr "O script de instalador %1 fallou, intentáronse os argumentos «%2»."

#: servicemenuinstaller.cpp:286
#, kde-format
msgid "Failed to create path %1"
msgstr "Non se puido crear a ruta %1"

#: servicemenuinstaller.cpp:300
#, kde-format
msgid "Failed to copy .desktop file %1 to %2: %3"
msgstr "Non se puido copiar o ficheiro .desktop %1 a %2: %3"

#: servicemenuinstaller.cpp:312 servicemenuinstaller.cpp:422
#, kde-format
msgid "Failed to remove directory %1"
msgstr "Non se puido retirar o directorio %1"

#: servicemenuinstaller.cpp:318
#, kde-format
msgid "Failed to create directory %1"
msgstr "Non se puido crear o directorio %1."

#: servicemenuinstaller.cpp:357
#, kde-format
msgid "Failed to find an installation script in %1"
msgstr "Non se puido atopar o script de instalación en %1"

#: servicemenuinstaller.cpp:371
#, kde-format
msgid "Failed to remove .desktop file %1: %2"
msgstr "Non se puido retirar o ficheiro .desktop %1: %2"

#: servicemenuinstaller.cpp:416
#, kde-format
msgid "Failed to find an uninstallation script in %1"
msgstr "Non se puido atopar un script de desinstalación en %1"

#: servicemenuinstaller.cpp:435
#, kde-format
msgctxt "@info:shell"
msgid "Command to execute: install or uninstall."
msgstr "Orde para executar: instalar ou desinstalar."

#: servicemenuinstaller.cpp:436
#, kde-format
msgctxt "@info:shell"
msgid "Path to archive."
msgstr "Ruta do arquivo."

#: servicemenuinstaller.cpp:441
#, kde-format
msgid "Command is required."
msgstr "A orde é necesaria."

#: servicemenuinstaller.cpp:444
#, kde-format
msgid "Path to archive is required."
msgstr "A ruta para arquivar é necesaria."

#: servicemenuinstaller.cpp:460
#, kde-format
msgid "Unsupported command %1"
msgstr "A orde %1 non está admitida"
